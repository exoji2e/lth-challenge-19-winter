#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using namespace std;
using ll = __int128_t;

constexpr long long SCALE = 10000000;

int main() {
	int n;
	cin >> n;
	
	vector<ll> pts;
	for (int i = 0; i < n; i++) {
		long long x, y;
		cin >> x >> y;
		pts.push_back((-x + y) * SCALE);
	}
	
	auto countDist = [&] (ll a) {
		ll dst = 0;
		for (ll p : pts) {
			ll d = p - a;
			dst += d * d;
		}
		return dst;
	};
	
	ll lo = -1E8 * SCALE;
	ll hi = 1E8 * SCALE;
	for (int it = 0; it < 100; it++) {
		ll l = (hi - lo) / 3;
		ll m1 = lo + l;
		ll m2 = lo + 2 * l;
		if (countDist(m1) < countDist(m2)) {
			hi = m2;
		} else {
			lo = m1;
		}
	}
	
	cout << setprecision(6) << fixed << (double)(hi + lo) / (2 * SCALE) << "\n";
}
