#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using namespace std;
using ld = long double;

int main() {
	int n;
	cin >> n;
	
	vector<ld> pts;
	for (int i = 0; i < n; i++) {
		int x, y;
		cin >> x >> y;
		pts.push_back(-x + y);
	}
	
	auto countDist = [&] (ld a) {
		ld dst = 0;
		for (ld p : pts) {
			ld d = p - a;
			dst += d * d;
		}
		return dst;
	};
	
	ld lo = -1E8;
	ld hi = 1E8;
	for (int it = 0; it < 100; it++) {
		ld l = (hi - lo) / 3;
		ld m1 = lo + l;
		ld m2 = lo + 2 * l;
		if (countDist(m1) < countDist(m2)) {
			hi = m2;
		} else {
			lo = m1;
		}
	}
	
	cout << setprecision(6) << fixed << (hi + lo) / 2 << "\n";
}
