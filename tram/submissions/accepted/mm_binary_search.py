from collections import *
import sys
from decimal import Decimal
inp = raw_input

def err(s):
    sys.stderr.write('{}\n'.format(s))

def ni():
    return int(inp())

def nl():
    return [int(_) for _ in inp().split()]

N = ni()
pts = []
for _ in range(N):
    x, y = nl()
    pts.append((y - x))

hi = max(pts)
lo = min(pts)
for _ in range(100):
    l = (hi - lo)/3.0
    mid1 = lo + l
    mid2 = lo + 2*l
    su1 = 0
    su2 = 0
    for p in pts:
        su1 += (mid1 - p)**2
        su2 += (mid2 - p)**2
    if su1 < su2:
        hi = mid2
    else:
        lo = mid1

print('{:.6f}'.format(lo))
#print(lo)
