from math import *
N = input()
out = 0.0
pts = [tuple(map(int,raw_input().split())) for _ in range(N)]
for x,y in pts: out += (y-x)
out /= N
print '{:.6f}'.format(out)
