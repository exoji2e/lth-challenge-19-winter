from math import *
def rotate(p):
    x,y = p
    m = (x**2 + y**2) ** 0.5
    v = atan2(y,x)
    v -= pi/4
    return (m*cos(v),m*sin(v))

N = input()
pts = [tuple(map(int,raw_input().split())) for _ in range(N)]
rotated_pts = [rotate(p) for p in pts]
a2 = sum(y for x,y in rotated_pts)/N
a = a2 * (2 ** 0.5)
print '{:.6f}'.format(a)
