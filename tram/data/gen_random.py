import sys
import random

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    assert default is not None, name
    return default

random.seed(int(cmdlinearg('seed',sys.argv[-1])))
N = int(cmdlinearg('N'))
print(N)
for _ in range(N):
    print(random.randint(-10**6,10**6),random.randint(-10**6,10**6))
