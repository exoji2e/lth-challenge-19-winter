#!/bin/bash
USE_SCORING=0
PPATH=$(realpath ..)
. ../../testdata_tools/gen-acm.sh

# For unlimited stack:
# ulimit -s unlimited

# Choose your solution
use_solution la_v2.py python2

# Compile generators
compile gen_random.py # Arguments: length seed

# Generate answers to sample cases (optional)
sample 1
sample 2
sample 3

# Note: tc automatically adds a deterministic, pseudo-random seed argument to your generator
#ex tc name_of_test gen_random inputsTogenrandom
tc random00 gen_random N=10
tc random01 gen_random N=100
tc random02 gen_random N=100
tc random03 gen_random N=1000
tc random04 gen_random N=1000
tc random05 gen_random N=1000
tc random06 gen_random N=1000
tc random07 gen_random N=1000
tc random08 gen_random N=10000
tc random09 gen_random N=10000
tc random10 gen_random N=10000
tc random11 gen_random N=100000
tc random12 gen_random N=100000
tc random13 gen_random N=100000
tc random14 gen_random N=100000
tc random15 gen_random N=100000
tc random16 gen_random N=100000
tc random17 gen_random N=100000
tc random18 gen_random N=100000
tc random19 gen_random N=100000
