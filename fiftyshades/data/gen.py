G = 3
N = 1000
L = 30

A = ''.join(chr(ord('a')+lt) for lt in range(26))
letters = list(A + A.upper())

import random

def get_random30():
    out = [random.choice(letters) for _ in range(30)]
    return out

def get_ok(p):
    return random.random() < p

def gen_file(g):
    random.seed(g)
    fname = 'secret/g{}.in'.format(g)
    with open(fname, 'w') as f:
        f.write(str(N) + '\n')
        for n in range(N):
            r30 = get_random30()
            if get_ok(0.1):
                add_str = 'pink' if get_ok(0.5) else 'rose'
                add_lst = list(add_str)
                for i in range(4):
                    if get_ok(0.5):
                        add_lst[i] = add_lst[i].upper()
                place = random.randint(0, 26)
                r30 = r30[:place] + add_lst + r30[place + 4:]
            f.write(''.join(r30)+ '\n')
    ansname = 'secret/g{}.ans'.format(g)
    with open(ansname, 'w') as f:
        f.write(str(solve(fname)) + '\n')

def solve(fname):
    lines = (line for line in open(fname,'r').read().split('\n'))
    N = int(next(lines))
    cnt = 0
    for n in range(N):
        col = next(lines).lower()
        if 'rose' in col or 'pink' in col:
            cnt +=1
    if cnt > 0:
        return cnt
    else:
        return "I must watch Star Wars with my daughter"

for g in range(G):
    gen_file(g)    
