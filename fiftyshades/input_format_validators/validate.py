#!/usr/bin/python
import sys,re

integer = "(0|-?[1-9]\d*)"
color = "[A-Za-z]*"

def get_ints(req):
    ln = sys.stdin.readline()
    assert re.match("^%s$" % (' '.join([integer] * req)), ln)
    nums = ln.split()
    assert len(nums) == req
    return map(int, nums)

ln = sys.stdin.readline()
assert re.match("^{}\n$".format(integer) , ln)
N = int(ln)

assert 1 <= N <= 10**3

for _ in range(N):
    ln = sys.stdin.readline()
    assert re.match("^{}$".format(color), ln)
    t = len(ln.strip())
    assert 1 <= t <= 30


assert sys.stdin.readline() == ''

# Nothing to report.
sys.exit(42)
