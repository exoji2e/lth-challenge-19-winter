import re, sys

text = sys.stdin.read().lower()
count =  len(re.findall('rose', text))
count += len(re.findall('pink', text))

print(count if count else "I must watch Star Wars with my daughter")
