N = int(raw_input())
cnt = 0
for n in range(N):
    col = raw_input().lower()
    if 'rose' in col or 'pink' in col:
        cnt +=1
if cnt > 0:
    print(cnt)
else:
    print("I must watch Star Wars with my daughter")