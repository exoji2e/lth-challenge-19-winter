#! python3
import re, sys
input()
colours = re.compile(r'(pink|rose)', re.I)
ct = sum(bool(colours.search(line)) for line in sys.stdin)
print (ct if ct > 0  else 'I must watch Star Wars with my daughter')
