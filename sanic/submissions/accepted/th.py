#! python3

# Clumsy solution that simulates the process using tau

from math import tau as τ

distance = τ  * float(input())
revolutions = 0
while distance >= τ:
    distance -= τ 
    revolutions += 1
assert 0 <= distance < τ
revolutions += distance / τ

print (revolutions - 1) # revolution revelation: one revolution is for free!
