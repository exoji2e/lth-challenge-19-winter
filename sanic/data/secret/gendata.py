import random
from os import system

random.seed(101)

lo, hi = 1.1, 1000
for tc in range(20):
    name = "%02d" % tc
    print(name)
    x = lo + random.random() * (hi - lo)
    with open(name + ".in", "w") as f:
        f.write("%.2f\n" % x)
    system("pypy ../../submissions/accepted/jo.py <%s.in >%s.ans" % (name, name))
