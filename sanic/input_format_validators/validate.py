#!/usr/bin/python
import sys,re

ln = sys.stdin.readline()
assert re.match("^(0|[1-9][0-9]*)\.([0-9][0-9])?$", ln)
r = float(ln)
assert 1.10 <= r <= 1000.00
assert sys.stdin.readline() == ''

# Nothing to report.
sys.exit(42)
