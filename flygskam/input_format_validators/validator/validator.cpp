#include "validator.h"
#include <unordered_set>
using namespace std;

constexpr int MAX_WEIGHT = 1E9;

string ExpectAirportCode() {
	string str = "---";
	for (char& c : str) {
		c = Char();
		assert(c >= 'A' && c <= 'Z');
	}
	return str;
}

void run() {
	int nMax = Arg("nMax");
	int mMax = Arg("mMax");
	
	int n = Int(1, nMax);
	Space();
	int m = Int(1, mMax);
	Endl();
	
	string st = ExpectAirportCode();
	Space();
	string ed = ExpectAirportCode();
	Endl();
	
	unordered_set<string> airportsSeen;
	for (int i = 0; i < n; i++) {
		string name = ExpectAirportCode();
		assert(airportsSeen.count(name) == 0);
		airportsSeen.insert(move(name));
		
		Space();
		Float(-90, 90, false);
		Space();
		Float(-180, 180, false);
		Endl();
	}
	
	unordered_set<string> edgesSeen;
	for (int i = 0; i < m; i++) {
		string name1 = ExpectAirportCode();
		Space();
		string name2 = ExpectAirportCode();
		Endl();
		
		assert(airportsSeen.count(name1));
		assert(airportsSeen.count(name2));
		assert(name1 != name2);
		
		if (name1 > name2)
			name1.swap(name2);
		assert(edgesSeen.emplace(name1 + "." + name2).second);
	}
	
	Eof();
}
