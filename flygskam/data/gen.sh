#!/usr/bin/env bash

USE_SCORING=0
. ../../testdata_tools/gen-acm.sh

ulimit -s unlimited

use_solution eae.cpp

compile gen_random.py

limits nMax=10000 mMax=100000

sample 1
sample 2
sample 3

tc rand0 gen_random n=2 m=1
tc rand1 gen_random n=2 m=1
tc rand2 gen_random n=6 m=8
tc rand3 gen_random n=12 m=12
tc rand4 gen_random n=25 m=40
tc rand5 gen_random n=20 m=30
tc rand6 gen_random n=200 m=3000
tc rand7 gen_random n=800 m=3000
tc rand8 gen_random n=1000 m=3000
tc rand9 gen_random n=1000 m=3000
tc rand10 gen_random n=1000 m=3000
tc rand11 gen_random n=5000 m=10000
tc rand12 gen_random n=5000 m=50500
tc rand13 gen_random n=5000 m=100000
tc rand14 gen_random n=6871 m=92222
tc rand15 gen_random n=8212 m=5900
tc rand16 gen_random n=10000 m=100000
tc rand17 gen_random n=10000 m=100000
