#!/bin/python3

import random, sys, math

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    assert default is not None, name
    return default

random.seed(int(cmdlinearg('seed', sys.argv[-1])))

airportNames = []
for a in range(65, 91):
    for b in range(65, 91):
        for c in range(65, 91):
            airportNames.append(chr(a) + chr(b) + chr(c))

n = int(cmdlinearg("n"))
m = int(cmdlinearg("m"))

airportNames = random.sample(airportNames, n)
random.shuffle(airportNames)

print(n, m)
print(random.choice(airportNames), random.choice(airportNames))
for airport in airportNames:
    lng = random.random() * 360 - 180
    lat = math.degrees(math.acos(random.random() * 2 - 1)) - 90
    print(f"{airport} {lat:.3f} {lng:.3f}")

edges = set()
while len(edges) < m:
    a = random.randrange(0, n)
    b = random.randrange(0, n)
    edge = (min(a, b), max(a, b))
    if a != b and not edge in edges:
        print(airportNames[a], airportNames[b])
        edges.add(edge)
