from math import cos, sin, acos, pi
from collections import defaultdict as defdict
import heapq

def rot_z(angle, v):
    x, y, z= v
    return [x * cos(angle) - y* sin(angle), x * sin(angle) + y * cos(angle), z]

def rot_y(angle, v):
    x, y, z= v
    return [x * cos(angle) + z* sin(angle), y, - x * sin(angle) + z* cos(angle)]

def rad(a):
    return a * pi/180 

def to_vec(lat, lo):
    v = [1.0, 0, 0]

    v =rot_z(rad(lat), v)
    v =rot_y(-rad(lo), v)
    return v

def angle_dist(v1, v2, R):
    dot = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    return R * acos(dot)

INF = 10**30

def inf(): return INF

def dij(g, S, T):
    # g lists with tuples (id, distance)
    # Dijkstra from S to T

    dist = defdict(inf)

    pq = []
    dist[S] = 0
    pq.append((0, S))
    heapq.heapify(pq)
    done = False
    while pq and not done:
        (ndist, node) = heapq.heappop(pq)
        if node == T: done= True
        for (nn, dd) in g[node]:
            alt = dist[node] + dd
            if dist[nn] > alt:
                dist[nn] = alt
                heapq.heappush(pq, (dist[nn], nn))
    return dist[T]

N, M = [int(v) for v in raw_input().split()]
R = 6381.0
ports = {}
S, T = raw_input().split()
for n in range(N):
    data = raw_input().split()
    name = data[0]
    la, lo = float(data[1]), float(data[2])
    ports[name] = to_vec(la, lo)

g = defdict(list)
for m in range(M):
    a, b = raw_input().split()
    d = angle_dist(ports[a], ports[b], R) + 100
    g[a].append((b, d))
    g[b].append((a, d))

mn = dij(g, S, T)
if mn == INF:
    print(-1)
else:
    print(mn)


