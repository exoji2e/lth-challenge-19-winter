from collections import *
import sys, math
inp = raw_input

def err(s):
    sys.stderr.write('{}\n'.format(s))

def ni():
    return int(inp())

def nl():
    return [int(_) for _ in inp().split()]

def deg2pt(latitude, longitude):
    latr = math.pi/2 - latitude*math.pi/180
    longr = longitude*math.pi/180
    return (math.sin(latr)*math.cos(longr), math.sin(latr)*math.sin(longr), math.cos(latr))

def ang(p, q):
    return math.acos(sum(p[i]*q[i] for i in range(3)))

def dist(crds1, crds2):
    r = 6381
    p1 = deg2pt(*crds1)
    p2 = deg2pt(*crds2)
    return ang(p1, p2)*r + 100


N, M = nl()
S, T = inp().split()
pos = {}
for _ in range(N):
    name, l1, l2 = inp().split()
    pos[name] = float(l1), float(l2)

adj = defaultdict(list)

for _ in range(M):
    A, B = inp().split()
    D = dist(pos[A], pos[B])
    adj[A].append((B, D))
    adj[B].append((A, D))

from heapq import *
INF = 10**18
dst = defaultdict(lambda:INF)
pq = []
def add(i, d):
    if dst[i] <= d: return
    dst[i] = d
    heappush(pq, (d, i))
add(S, 0)
while pq:
    d, i = heappop(pq)
    if i == T:
        print(d)
        exit()
    for j, w in adj[i]:
        add(j, d + w)
print(-1)
