#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using namespace std;
using ld = long double;

struct Node {
	vector<pair<int, ld>> edges;
	ld minDist = INFINITY;
	ld x, y, z;
};

int main() {
	int n, m;
	cin >> n >> m;
	
	string startS, endS;
	cin >> startS >> endS;
	
	vector<Node> nodes(n);
	unordered_map<string, int> airportNameToIdx;
	
	for (int i = 0; i < n; i++) {
		string name;
		ld lat, lng;
		cin >> name >> lat >> lng;
		lat *= M_PI / 180;
		lng *= M_PI / 180;
		nodes[i].x = cos(lng) * cos(lat);
		nodes[i].y = sin(lat);
		nodes[i].z = sin(lng) * cos(lat);
		airportNameToIdx.emplace(move(name), i);
	}
	
	for (int i = 0; i < m; i++) {
		string a, b;
		cin >> a >> b;
		int ai = airportNameToIdx.at(a);
		int bi = airportNameToIdx.at(b);
		ld dot = nodes[ai].x * nodes[bi].x + nodes[ai].y * nodes[bi].y + nodes[ai].z * nodes[bi].z;
		ld dist = acos(dot) * 6381 + 100;
		nodes[ai].edges.emplace_back(bi, dist);
		nodes[bi].edges.emplace_back(ai, dist);
	}
	
	int startI = airportNameToIdx.at(startS);
	int endI = airportNameToIdx.at(endS);
	
	priority_queue<pair<ld, int>> pq;
	pq.emplace(0, startI);
	nodes[startI].minDist = 0;
	
	while (!pq.empty()) {
		int cur = pq.top().second;
		pq.pop();
		
		if (cur == endI) {
			cout << setprecision(15) << nodes[endI].minDist << "\n";
			return 0;
		}
		
		for (auto edge : nodes[cur].edges) {
			ld ndst = nodes[cur].minDist + edge.second;
			if (nodes[edge.first].minDist > ndst) {
				nodes[edge.first].minDist = ndst;
				pq.emplace(-ndst, edge.first);
			}
		}
	}
	
	cout << -1 << "\n";
}
