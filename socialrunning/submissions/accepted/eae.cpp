#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using namespace std;

int main() {
	int n;
	cin >> n;
	
	vector<int> dist(n);
	for (int i = 0; i < n; i++) {
		cin >> dist[i];
	}
	
	int ans = INT_MAX;
	for (int i = 0; i < n; i++) {
		ans = min(ans, dist[i] + dist[(i + 2) % n]);
	}
	
	cout << ans << "\n";
}
