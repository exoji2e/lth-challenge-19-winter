try:
    inp = raw_input
except:
    inp = input
def ni():
    return int(inp())

n = ni()
V = [ni() for _ in range(n)]

MIN = 10**10
for i in range(n):
    MIN = min(MIN, V[i] + V[i-2])
print(MIN)
