#! python3
import sys

class Runner:
    def __init__(self, home):
        self.state = 'waiting eagerly'
        self.home = home

    def __str__(self):
        return str(self.home)


n = int(input())

d = [int (line) for line in sys.stdin]
d = d + d

total_solitary_distances = []

for start in range(n):
    runners = []
    for i in range(n):
        runners.append(Runner(i))
    group = []
    here = start
    this_distance = 0
    while not all(r.state == 'proud and exhausted' for r in runners):
        for r in runners:
            if r.home == here and r.state == 'running':
                #sys.stderr.write(str(r) + ' leaves\n')
                group.remove(r)
                r.state = 'proud and exhausted'
            if r.home == here and r.state == 'waiting eagerly':
                #sys.stderr.write(str(r) + ' joins ' + str(group) +'\n')
                r.state = 'running'
                group.append(r)
        if len(group) == 1:
            this_distance += d[here]
        here += 1
        if here >= n:
            here = 0
    total_solitary_distances.append(this_distance)

print (min(total_solitary_distances))

