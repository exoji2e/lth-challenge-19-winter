#! python3
d = list(map(int, (input() for _ in range(int(input())))))
print (min(x + y for x, y in zip(d, d[2:] + d[:2])))
