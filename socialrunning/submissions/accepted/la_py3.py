#!usr/bin/env python3
N = int(input())
d = [int(input()) for _ in range(N)]
mi = 10**20
for i in range(N):
    mi = min(mi, d[i]+d[i-2])
print(mi)
