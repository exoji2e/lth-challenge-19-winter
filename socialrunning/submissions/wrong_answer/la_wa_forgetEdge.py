N = input()
d = [input() for _ in range(N)]
mi = 10**20
for i in range(N-2):
    mi = min(mi, d[i]+d[i+2])
print mi
