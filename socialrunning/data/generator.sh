#!/usr/bin/env bash

USE_SCORING=0
. ../../testdata_tools/gen-acm.sh

# For unlimited stack:
# ulimit -s unlimited

# Choose your solution
use_solution la_py2.py python2

# Compile generators
compile gen_random.py # Arguments: length seed
compile gen_break_edge.py # Arguments: length seed

# Generate answers to sample cases (optional)
sample 1

# Note: tc automatically adds a deterministic, pseudo-random seed argument to your generator
#ex tc name_of_test gen_random inputsTogenrandom
tc random00 gen_random N=10 dMax=10000
tc random01 gen_random N=10 dMax=10000
tc random02 gen_random N=10 dMax=10000
tc random03 gen_random N=10 dMax=10000
tc random04 gen_random N=9 dMax=10000
tc random05 gen_random N=8 dMax=10000
tc random06 gen_random N=7 dMax=10000
tc random07 gen_random N=6 dMax=10000
tc random08 gen_random N=5 dMax=10000
tc random09 gen_random N=4 dMax=10000
tc random10 gen_random N=3 dMax=10000
tc random11 gen_random N=2 dMax=10000
tc random12 gen_random N=10 dMax=10000
tc random13 gen_random N=10 dMax=10000
tc random14 gen_random N=10 dMax=10000
tc random15 gen_random N=10 dMax=10000
tc random16 gen_random N=9 dMax=10000
tc random17 gen_random N=8 dMax=10000
tc random18 gen_random N=7 dMax=10000
tc random19 gen_random N=6 dMax=10000
tc random20 gen_random N=5 dMax=10000
tc random21 gen_random N=4 dMax=10000
tc random22 gen_random N=3 dMax=10000
tc random23 gen_random N=2 dMax=10000
tc break0 gen_break_edge N=10 dMax=10000 toBreak=0
tc break1 gen_break_edge N=10 dMax=10000 toBreak=9
tc break2 gen_break_edge N=9 dMax=10000 toBreak=0
tc break3 gen_break_edge N=9 dMax=10000 toBreak=8
tc break4 gen_break_edge N=8 dMax=10000 toBreak=0
tc break5 gen_break_edge N=8 dMax=10000 toBreak=7
tc break6 gen_break_edge N=7 dMax=10000 toBreak=0
tc break7 gen_break_edge N=7 dMax=10000 toBreak=6
