import sys
import random

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    assert default is not None, name
    return default

N = int(cmdlinearg('N',10))
dMax = int(cmdlinearg('dMax',10000))
toBreak = int(cmdlinearg('toBreak',0))
print(N)
L = [dMax]*N
L[toBreak] = 1
L[toBreak-2] = 1
print('\n'.join(map(str,L)))
