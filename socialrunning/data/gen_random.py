import sys
import random

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    assert default is not None, name
    return default

random.seed(int(cmdlinearg('seed',sys.argv[-1])))
N = int(cmdlinearg('N',10))
dMax = int(cmdlinearg('dMax',10000))
print(N)
for _ in range(N): print(random.randint(1,dMax))
