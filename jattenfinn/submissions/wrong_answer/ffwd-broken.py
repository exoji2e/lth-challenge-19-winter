from __future__ import print_function
from sys import stdin
n = int(stdin.readline())
s = list(int(x) for x in stdin.readline().split())
w = [1000] * n 
ffwd = [ None ] * n

def damage(i):
    support = s[:]
    weight = w[:]
    damage = 1
    l = i - 1
    r = i + 1
    if 0 < i:
        weight[l] += 500
    if i < n - 1:
        weight[r] += 500
    stable = False
    while (not stable):
        verbose = False
        if False:
            verbose = True
            print(l,i, r, ffwd[i])
        #print (support, '\n', weight, l, r)
        stable = True
        if l >= 0 and support[l] < weight[l]:
            if verbose: print ('l')
            stable = False
            if ffwd[l] is not None:
                l_ = ffwd[l][0]
                assert l_ < l
                damage += l - l_
                l = l_
            else:
                damage += 1 
                l -= 1
        if r < n and support[r] < weight[r]:
            if verbose: print ('r')
            stable = False
            if ffwd[r] is not None:
                r_ = ffwd[r][1]
                assert r_ > r
                damage += r - r_
                r = r_
            else:
                damage += 1
                r += 1
        dist = r - l  
        if l >= 0:
            weight[l] = 500 * dist + 500
        if r < n:
            weight[r] = 500 * dist + 500
    for j in range(i, min(r, n-1)):
        if verbose: print ('.')
        ffwd[j] = (l, r)
    return damage, r


#print (' '.join(list(str(damage(i)) for i in range(n))))
maxd = 0
i = 0
while i < n:
    d, nexti = damage(i)
    if d > maxd:
        maxd, maxi = d, i
    assert nexti > i
    i = nexti

print (maxd, maxi)



    
