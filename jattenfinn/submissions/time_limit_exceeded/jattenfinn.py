from __future__ import print_function
from sys import stdin
n = int(stdin.readline())
s = list(int(x) for x in stdin.readline().split())
w = [1000] * n 

def damage(i):
    support = s[:]
    weight = w[:]
    damage = 1
    support[i] = weight[i] = 0
    l = i - 1
    r = i + 1
    if 0 < i:
        weight[l] += 500
    if i < n - 1:
        weight[r] += 500
    stable = False
    while (not stable):
        #print (support, '\n', weight, l, r)
        stable = True
        if l >= 0 and support[l] < weight[l]:
            stable = False
            damage += 1 
            support[l] = weight[l] = 0
            l -= 1
        if r < n and support[r] < weight[r]:
            stable = False
            damage += 1 
            support[r] = weight[r] = 0
            r += 1
        dist = r - l  
        if l >= 0:
            weight[l] = 500 * dist + 500
        if r < n:
            weight[r] = 500 * dist + 500
    return damage


#print (' '.join(list(str(damage(i)) for i in range(n))))
#print (max(damage(i) for i in range(n)))
maxd, maxi = 0, None
for i in range(n):
    d = damage(i)
    if d > maxd:
        maxd, maxi = d, i
print (maxd, maxi)
