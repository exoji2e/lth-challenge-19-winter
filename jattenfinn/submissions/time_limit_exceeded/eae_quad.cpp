#pragma GCC optimize("Ofast", "unroll-loops")
#include <bits/stdc++.h>
using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	
	int n;
	cin >> n;
	
	vector<int> capacity(n);
	for (int i = 0; i < n; i++)
		cin >> capacity[i];
	
	pair<int, int> ans(1, 0);
	
	for (int i = 0; i < n; i++) {
		int lo = i - 1;
		int hi = i + 1;
		while (true) {
			int halfW = (1 + hi - lo) * 500;
			if (lo >= 0 && halfW > capacity[lo]) {
				lo--;
			} else if (hi < n && halfW > capacity[hi]) {
				hi++;
			} else {
				break;
			}
		}
		ans = max(ans, make_pair(hi - lo - 1, i));
	}
	
	cout << ans.first << " " << ans.second << "\n";
}
