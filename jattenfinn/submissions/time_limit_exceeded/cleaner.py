from __future__ import print_function
from sys import stdin
n = int(stdin.readline())
s = list(int(x) for x in stdin.readline().split())
w = [1000] * n 

def damage(i):
    support = s[:]
    weight = w[:]
    support[i] = 0
    damage = -1 # hacky. gets increased by 2 in a moment. sue me
    l = r = i
    stable = False
    while (not stable):
        #print (support, '\n', weight, l, r)
        stable = True
        if l >= 0 and support[l] < weight[l]:
            stable = False
            damage += 1 
            l -= 1
        if r < n and support[r] < weight[r]:
            stable = False
            damage += 1 
            r += 1
        dist = r - l  
        if l >= 0:
            weight[l] = 500 * dist + 500
        if r < n:
            weight[r] = 500 * dist + 500
    return damage


#print (' '.join(list(str(damage(i)) for i in range(n))))
maxd, maxi = 0, None
for i in range(n):
    d = damage(i)
    if d > maxd:
        maxd, maxi = d, i
print (maxd, maxi)
