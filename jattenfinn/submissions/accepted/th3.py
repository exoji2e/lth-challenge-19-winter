#! python3
n = int(input())
support = [int(x) for x in input().split()]

prev = list(range(-1, n - 1))
maxdamage = i = 0
while i < n:
    l, r  = i - 1, i + 1    
    stable = False
    while (not stable):
        stable = True
        if l >= 0 and support[l] < 500 * (r - l) + 500:
            stable = False
            l = prev[l]
        if r < n and support[r] < 500 * (r - l) + 500:
            stable = False
            r += 1
    if r - l - 1 > maxdamage:
        maxdamage, maxi = r - l - 1, i
    prev[i], i  = l, r

print (maxdamage, maxi)



    
