#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using namespace std;
using ll = long long;

struct SparseTable {
	using T = ll;
	ll node(ll l, ll i) { return i + l * n; }
	ll n; vector<T> v;
	SparseTable(vector<T> values) : n(values.size()), v(move(values)) {
		ll d = log2(n);
		v.resize((d + 1) * n);
		for (ll L = 0, s = 1; L < d; L++, s *= 2) {
			for (ll i = 0; i < n; i++) {
				v[node(L + 1, i)] = max(v[node(L, i)], v[node(L, min(i + s, n - 1))]);
			}
		}
	}
	T query(ll lo, ll hi) {
		if (lo >= hi) return 0;
		ll l = (ll)log2(hi - lo);
		return max(v[node(l, lo)], v[node(l, hi - (1 << l))]);
	}
};

int main() {
	ll n; cin >> n;
	
	vector<ll> capacity(n);
	vector<ll> rmqValues(n);
	for (ll i = 0; i < n; i++) {
		cin >> capacity[i];
		rmqValues[i] = capacity[i] - 500 * i;
	}
	
	SparseTable rmq(move(rmqValues));
	
	pair<ll, ll> ans(1, 0);
	pair<ll, ll> prevBest;
	
	for (ll st = n - 1; st >= 0; st--) {
		pair<ll, ll> best(1, st);
		
		if (st < n - 1) {
			auto [nextMx, nextI] = prevBest;
			if (1000 + nextMx * 500 > capacity[st]) {
				best = { nextMx + 1, nextI };
			}
		}
		
		ll lo = st + best.first;
		ll hi = n;
		while (hi > lo) {
			ll mid = lo + (hi - lo) / 2;
			if (rmq.query(st + best.first, min(mid + 1, n)) >= 1000 - st * 500)
				hi = mid;
			else
				lo = mid + 1;
		}
		best.first = lo - st;
		
		ans = max(ans, best);
		prevBest = best;
	}
	
	cout << ans.first << " " << ans.second << "\n";
}
