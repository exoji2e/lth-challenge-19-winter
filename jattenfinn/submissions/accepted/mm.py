from collections import *
import sys
inp = raw_input

def err(s):
    sys.stderr.write('{}\n'.format(s))

def ni():
    return int(inp())

def nl():
    return [int(_) for _ in inp().split()]

N = ni()
B = nl()

killed = [0 for _ in range(N)]

best = 0, 0

for i in range(N):
    if killed[i]: continue
    mass = 1500
    l, r = i-1, i+1
    ch = True
    while ch:
        ch = False
        if l != -1 and mass > B[l]:
            killed[l] += 1
            ch = True
            mass += 500
            l -= 1
        if r != N and mass > B[r]:
            killed[r] += 1
            ch = True
            mass += 500
            r += 1
    best = max(best, (r - l - 1,i))
print('{} {}'.format(best[0], best[1]))

