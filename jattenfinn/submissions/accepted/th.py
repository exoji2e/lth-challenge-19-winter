from __future__ import print_function
from sys import stdin
n = int(stdin.readline())
support = list(int(x) for x in stdin.readline().split())
prev = list(range(-1,n-1))

maxd = 0
i = 0
while i < n:
    damage = 1
    l, r  = i - 1, i + 1    
    stable = False
    while (not stable):
        stable = True
        if l >= 0 and support[l] < 500 * (r - l) + 500:
            stable = False
            damage += l - prev[l]
            l = prev[l]
        if r < n and support[r] < 500 * (r - l) + 500:
            stable = False
            damage += 1
            r += 1
    prev[i] = l
    if damage > maxd:
        maxd, maxi = damage, i
    i = r

print (maxd, maxi)



    
