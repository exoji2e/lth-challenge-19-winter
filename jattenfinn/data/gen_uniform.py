# n pillars with weight m

from util import *

n = int(cmdlinearg('n'))
m = int(cmdlinearg('m'))

print(n)
print (' '.join([str(m) for _ in range(n)]))
