from __future__ import print_function
from collections import defaultdict
import os
from sys import stdin

def damage(s, i):
    support = s[:]
    weight = [1000] * len(s)
    support[i] = 0
    damage = -1 # hacky. gets increased by 2 in a moment. sue me
    l = r = i
    stable = False
    while (not stable):
        #print (support, '\n', weight, l, r)
        stable = True
        if l >= 0 and support[l] < weight[l]:
            stable = False
            damage += 1 
            l -= 1
        if r < n and support[r] < weight[r]:
            stable = False
            damage += 1 
            r += 1
        dist = r - l  
        if l >= 0:
            weight[l] = 500 * dist + 500
        if r < n:
            weight[r] = 500 * dist + 500
    return damage, i 

print ('Statistics for input files. What is the max damage, and how many solutions are there?')
print ('Also, what is the 2nd best answer, etc.\n')
print ('{:20}{:>6}{:>8}{:>6} | {:>8}{:>6}'.format('Filename', 'n', 'max dmg', '#sols', '2nd dmg', '...'))
print ('-'*80)
for filename in sorted(os.listdir('secret')):
    if not filename.endswith('.in'):
        continue
    with open('secret/'+filename, 'r') as f:
        n = int(f.readline())
        s = list(int(x) for x in f.readline().split())

    D = defaultdict(int)
    maxd, maxi = 0, None
    for i in range(n):
        d, _ = damage(s, i)
        D[d] += 1

    print ('{:20}{:6d}'.format(filename, n), end = '')
    m =  max(D.keys())
    shown = 0
    for i in range(m, -1, -1):
        if i in D:
            print ('{:8d}{:6d} | '.format(i, D[i]), end = '')
            shown += 1
        if shown == 5:
            break
    print()

