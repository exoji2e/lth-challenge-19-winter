from collections import deque
from util import *

# n columns that break up into r parts of random-ish length

n = int(cmdlinearg('n'))
r = int(cmdlinearg('r'))
assert 0 < r < n/2

print(n)

splits = sorted(random.sample(range(n-r), r-1)) # r positions out of n
splits.append(n)
splits = [0] + splits
res = []
for i in range(r):
    nn = splits[i+1] - splits[i] - 1
    index = nn//2 
    columns = deque()
    for i in range(1, nn):
        columns.append(1000 + 500 * i - 1)
    s = [None] * nn
    s[index] = 5000000 # this is what Finn will destroy

    l = index - 1
    r = index + 1
    while (l >= 0 or r <= nn - 1):
        if r <= nn - 1:
            s[r] = columns.popleft()
            r += 1
        if l >= 0:
            s[l] = columns.popleft()
            l -= 1
    res.extend(s)
    res.append(10000000)

print (' '.join(list(map(str, res))))
