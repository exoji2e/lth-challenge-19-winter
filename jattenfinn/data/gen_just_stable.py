from collections import deque
from util import *

# n columns that are just exactly stable (or not), with minimal tolerance
# with p = 0: columns are exctly stable (so none will fall)
# with p = 1: columnns are exactly unstable (so all will fall)
# for 0 < p < 1, randomly roughly p fraction will fall

index = int(cmdlinearg('i'))
n = int(cmdlinearg('n'))
p = float(cmdlinearg('p'))
assert 0 <= index < n

print(n)

columns = deque()
defect = 1 if p == 1 else 0
for i in range(1, n):
    defect = int(i / n < p)
    columns.append(1000 + 500 * i - defect)


s = [None] * n
s[index] = 10000 # this is what Finn will destroy

l = index - 1
r = index + 1
while (l >= 0 or r <= n - 1):
    if r <= n - 1:
        s[r] = columns.popleft()
        r += 1
    if l >= 0:
        s[l] = columns.popleft()
        l -= 1


print (' '.join(list(map(str, s))))


