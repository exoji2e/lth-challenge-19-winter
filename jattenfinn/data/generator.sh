#!/usr/bin/env bash

USE_SCORING=0
. ../../testdata_tools/gen.sh

use_solution th.py

compile gen_random.py
compile gen_uniform.py
compile gen_just_stable.py
compile gen_parts.py

sample 1
sample 2

tc random-1-2 gen_random n=10 m=5000
tc random-1-3 gen_random n=10 m=6000
tc random-1-4 gen_random n=10 m=7000
tc random-1-5 gen_random n=10 m=10000
tc random-1-6 gen_random n=10 m=20000
tc random-1-7 gen_random n=10 m=50000
tc random-1-8 gen_random n=10 m=100000
tc random-1-9 gen_random n=10 m=500000
tc stable-1-1 gen_just_stable n=10 p=.33 i=0
tc stable-1-2 gen_just_stable n=10 p=.33 i=9
tc stable-1-3 gen_just_stable n=10 p=.33 i=5

tc random-3-2 gen_random n=1000 m=5000
tc random-3-3 gen_random n=1000 m=6000
tc random-3-4 gen_random n=1000 m=7000
tc random-3-5 gen_random n=1000 m=10000
tc random-3-6 gen_random n=1000 m=20000
tc random-3-7 gen_random n=1000 m=50000
tc random-3-8 gen_random n=1000 m=100000
tc random-3-9 gen_random n=1000 m=500000
tc stable-3-l-3 gen_just_stable n=1000 p=.33 i=0
tc stable-3-r-3 gen_just_stable n=1000 p=.33 i=999
tc stable-3-m-3 gen_just_stable n=1000 p=.33 i=500
tc stable-3-l-0 gen_just_stable n=1000 p=0 i=0
tc stable-3-r-0 gen_just_stable n=1000 p=0 i=999
tc stable-3-m-0 gen_just_stable n=1000 p=0 i=500
tc stable-3-l-1 gen_just_stable n=1000 p=1 i=0
tc stable-3-r-1 gen_just_stable n=1000 p=1 i=999
tc stable-3-m-1 gen_just_stable n=1000 p=1 i=500
tc parts-3-1 gen_parts n=1000 r=5
tc parts-3-2 gen_parts n=1000 r=5
tc parts-3-3 gen_parts n=1000 r=5
tc parts-3-4 gen_parts n=1000 r=5

tc random-4-1 gen_random n=10000 m=2000
tc random-4-2 gen_random n=10000 m=5000
tc random-4-3 gen_random n=10000 m=6000
tc random-4-4 gen_random n=10000 m=7000
tc random-4-5 gen_random n=10000 m=10000
tc random-4-6 gen_random n=10000 m=20000
tc random-4-7 gen_random n=10000 m=50000
tc random-4-8 gen_random n=10000 m=100000
tc random-4-9 gen_random n=10000 m=500000
tc stable-4-l-3 gen_just_stable n=10000 p=.33 i=0
tc stable-4-r-3 gen_just_stable n=10000 p=.33 i=9999
tc stable-4-m-3 gen_just_stable n=10000 p=.33 i=5000
tc stable-4-l-0 gen_just_stable n=10000 p=0 i=0
tc stable-4-r-0 gen_just_stable n=10000 p=0 i=9999
tc stable-4-m-0 gen_just_stable n=10000 p=0 i=5000
tc stable-4-l-1 gen_just_stable n=10000 p=1 i=0
tc stable-4-r-1 gen_just_stable n=10000 p=1 i=9999
tc stable-4-m-1 gen_just_stable n=10000 p=1 i=5000
tc parts-4-1 gen_parts n=10000 r=50
tc parts-4-2 gen_parts n=10000 r=50
tc parts-4-3 gen_parts n=10000 r=50
tc parts-4-4 gen_parts n=10000 r=50

tc random-5-1 gen_random n=100000 m=2000
tc random-5-2 gen_random n=100000 m=3000
tc random-5-3 gen_random n=100000 m=4000
tc random-5-4 gen_random n=100000 m=5000
tc random-5-5 gen_random n=100000 m=6000
tc random-5-6 gen_random n=100000 m=7000
tc stable-5-l-3 gen_just_stable n=100000 p=.33 i=0
tc stable-5-r-3 gen_just_stable n=100000 p=.33 i=99999
tc stable-5-m-3 gen_just_stable n=100000 p=.33 i=5000
tc stable-5-l-0 gen_just_stable n=100000 p=0 i=0
tc stable-5-r-0 gen_just_stable n=100000 p=0 i=99999
tc stable-5-m-0 gen_just_stable n=100000 p=0 i=5000
tc stable-5-l-1 gen_just_stable n=100000 p=1 i=0
tc stable-5-r-1 gen_just_stable n=100000 p=1 i=99999
tc stable-5-m-1 gen_just_stable n=100000 p=1 i=5000
tc parts-5-1 gen_parts n=100000 r=50
tc parts-5-2 gen_parts n=100000 r=50
tc parts-5-3 gen_parts n=100000 r=50
tc parts-5-4 gen_parts n=100000 r=50
