# n random pillars with weight 1000,..., m-1

from util import *
from random import randrange

n = int(cmdlinearg('n'))
m = int(cmdlinearg('m'))

print(n)
print (' '.join([str(randrange(1000, m)) for _ in range(n)]))
