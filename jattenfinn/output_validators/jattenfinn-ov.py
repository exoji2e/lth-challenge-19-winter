# usage: ./jattenfinn-ov.py input.in output.ans tmpdir < team-answer.txt
from __future__ import print_function
from sys import argv, stdin

with open(argv[1]) as infile:
    n = int(infile.readline())
    support = list(map(int, infile.readline().split()))
with open(argv[2]) as ansfile:
    correct_size, _ = map(int, ansfile.readline().split())

if not stdin:
    print("WA: empty output")
    exit(43)
try:
    size, i = map(int,stdin.readline().split())
except ValueError as e:
    print("WA: int expected")
    exit(43)
if stdin.readline():
    print("WA: too much output")
    exit(43)

weight = [1000] * n
damage = 1
l = i - 1
r = i + 1
if 0 < i:
    weight[l] += 500
if i < n - 1:
    weight[r] += 500
stable = False
while (not stable):
    #print (support, '\n', weight, l, r)
    stable = True
    if l >= 0 and support[l] < weight[l]:
        stable = False
        damage += 1 
        l -= 1
    if r < n and support[r] < weight[r]:
        stable = False
        damage += 1 
        r += 1
    dist = r - l  
    if l >= 0:
        weight[l] = 500 * dist + 500
    if r < n:
        weight[r] = 500 * dist + 500

if damage != correct_size:
   print ("WA: wrong damage. Expected {} got {}".format(damage, correct_size))
   exit(43)
elif size != damage:
   print ("WA: wrong damage. Is {}, claimed {}".format(damage, size))
   exit(43)

exit(42)







