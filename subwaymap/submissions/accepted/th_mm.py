import math
import sys
try:
    inp = raw_input
except:
    inp = input
inp = (line for line in sys.stdin.read().strip().split('\n'))

n, m = map(int, next(inp).split())
logn = 2 + int(math.log(n,2))
E = []
adj = [[] for _ in range(n)]

dist = {}

for _ in range(m):
    a, b, l, c = map(lambda s: 0 if s == "?" else int(s), next(inp).split())
    a, b = min(a,b) - 1, max(a,b) - 1
    E.append((a,b,l,c))
    if c:
        adj[a].append(b)
        adj[b].append(a)
        dist[(a,b)] = l if l > 0 else 1
        dist[(b,a)] = l if l > 0 else 1

par = [[None] * logn for _ in range(n)] 
depth = [None] * n
length  = [None] * n

def run_dfs():
    cmds = [(0, None)]
    while cmds:
        v, p = cmds.pop()
        par[v][0] = p
        if p is not None:
            depth[v] = depth[p] + 1
        i = 1
        while par[v][i - 1] is not None:
            pi = par[v][i-1]
            pii = par[pi][i-1]
            if pii != None:
                d1 = dist[v, pi]
                d2 = dist[pi, pii]
                dist[v, pii] = max(d1, d2)
            par[v][i] = pii
            i += 1
        for u in adj[v]:
            if u != p:
                cmds.append((u, v))

def lca(v, u):
    if depth[v] < depth[u]:
        v, u = u, v
    assert depth[v] >= depth[u]
    for i in reversed(range(logn)):
        if par[v][i] is not None and depth[par[v][i]] >= depth[u]:
            v = par[v][i]
    assert depth[v] == depth[u]
    if v == u:
        return v
    for i in reversed(range(logn)):
        if par[v][i] !=  par[u][i]:
                v = par[v][i]
                u = par[u][i]
    return par[v][0];

depth[0] = 0

run_dfs()

out = []
for a, b, l, c in E:
    if l != 0: continue
    if c == 1:
        out.append("1")
        continue

    l = lca(a,b)
    L = []
    for i in reversed(range(logn)):
        p = par[a][i]
        if p != None and depth[p] >= depth[l]:
            L.append(dist[a,p])
            a = p
    for i in reversed(range(logn)):
        p = par[b][i]
        if p != None and depth[p] >= depth[l]:
            L.append(dist[b,p])
            b = p
    out.append(str(max(L)))
print('\n'.join(out))
#print (par)

#print (depth)

