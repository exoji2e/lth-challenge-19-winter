n, m = map(int, raw_input().split())

MAX_LOG2_N = 18

nodeDepth = [0] * n
adj = [[] for i in range(n)]
lift = [[] for i in range(n)]
unknownEdges = []

for i in range(m):
	a, b, w, inMST = raw_input().split()
	a = int(a) - 1
	b = int(b) - 1
	inMST = inMST == "1"
	if w == "?":
		w = int(inMST)
		unknownEdges.append((a, b, inMST))
	adj[a].append((b, int(w), inMST))
	adj[b].append((a, int(w), inMST))

dfsStack = [0]
dfsVis = [False] * n
dfsVis[0] = True
lift[0].append((0, 0))
while dfsStack:
	cur = dfsStack.pop()
	for nx, w, inMST in adj[cur]:
		if inMST and not dfsVis[nx]:
			nodeDepth[nx] = nodeDepth[cur] + 1
			lift[nx].append((cur, w))
			dfsVis[nx] = True
			dfsStack.append(nx)

for d in range(1, MAX_LOG2_N):
	for i in range(n):
		midNode, loWeight = lift[i][d - 1]
		endNode, hiWeight = lift[midNode][d - 1]
		lift[i].append((endNode, max(loWeight, hiWeight)))

for n1, n2, inMST in unknownEdges:
	if inMST:
		print(1)
	else:
		w = 0
		u, v = n1, n2
		if nodeDepth[u] < nodeDepth[v]:
			u, v = v, u
		for d in range(MAX_LOG2_N - 1, -1, -1):
			if nodeDepth[u] - 2 ** d >= nodeDepth[v]:
				w = max(w, lift[u][d][1])
				u = lift[u][d][0]
		if u != v:
			for d in range(MAX_LOG2_N - 1, -1, -1):
				if lift[u][d][0] != lift[v][d][0]:
					w = max(w, max(lift[u][d][1], lift[v][d][1]))
					u = lift[u][d][0]
					v = lift[v][d][0]
			w = max(w, max(lift[u][0][1], lift[v][0][1]))
		print(w)
