#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using ll = long long;
using namespace std;

struct Edge {
	ll a, b, weight;
	bool unknown, inMST;
};

struct Node {
	vector<pair<ll, Edge*>> edges;
	ll depth = 0;
};

vector<Node> nodes;
vector<Edge> edges;

constexpr ll MAX_LOG2_N = 18;
pair<ll, ll> lift[100001][MAX_LOG2_N];

void mstDFS(ll i, ll p) {
	for (auto e : nodes[i].edges) {
		if (e.second->inMST && e.first != p) {
			nodes[e.first].depth = nodes[i].depth + 1;
			lift[e.first][0] = make_pair(i, e.second->weight);
			mstDFS(e.first, i);
		}
	}
}

int main() {
	ll n, m;
	cin >> n >> m;
	
	nodes.resize(n);
	edges.resize(m);
	
	for (ll i = 0; i < m; i++) {
		string weight;
		cin >> edges[i].a >> edges[i].b >> weight >> edges[i].inMST;
		edges[i].a--;
		edges[i].b--;
		
		nodes[edges[i].a].edges.emplace_back(edges[i].b, &edges[i]);
		nodes[edges[i].b].edges.emplace_back(edges[i].a, &edges[i]);
		
		edges[i].unknown = weight == "?";
		if (edges[i].unknown) {
			edges[i].weight = edges[i].inMST;
		} else {
			edges[i].weight = stol(weight);
		}
	}
	
	mstDFS(0, -1);
	
	for (ll d = 1; d < MAX_LOG2_N; d++) {
		for (ll i = 0; i < n; i++) {
			ll midNode = lift[i][d - 1].first;
			lift[i][d] = make_pair(
				lift[midNode][d - 1].first,
				max(lift[i][d - 1].second, lift[midNode][d - 1].second)
			);
		}
	}
	
	for (const Edge& e : edges) {
		if (!e.unknown)
			continue;
		
		ll w = e.weight;
		if (!e.inMST) {
			ll u = e.a, v = e.b;
			if (nodes[u].depth < nodes[v].depth)
				swap(u, v);
			
			for (ll d = MAX_LOG2_N - 1; d >= 0; d--) { 
				if (nodes[u].depth - (1LL << d) >= nodes[v].depth) {
					w = max(w, lift[u][d].second);
					u = lift[u][d].first;
				}
			}
			
			if (u != v) {
				for (ll d = MAX_LOG2_N - 1; d >= 0; d--) {
					if (lift[u][d].first != lift[v][d].first) {
						w = max(w, max(lift[u][d].second, lift[v][d].second));
						u = lift[u][d].first;
						v = lift[v][d].first;
					}
				}
				w = max(w, max(lift[u][0].second, lift[v][0].second));
			}
		}
		
		cout << w << "\n";
	}
}
