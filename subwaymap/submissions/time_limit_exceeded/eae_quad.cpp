#include <bits/stdc++.h>
#define all(x) begin(x),end(x)
using ll = long long;
using namespace std;

struct Edge {
	ll a, b, weight;
	bool unknown, inMST;
};

struct Node {
	vector<pair<ll, Edge*>> edges;
};

vector<Node> nodes;
vector<Edge> edges;

ll maxInPath(ll a, ll b, ll p) {
	if (a == b)
		return 0;
	for (auto e : nodes[a].edges) {
		if (e.first != p && e.second->inMST) {
			ll mx = maxInPath(e.first, b, a);
			if (mx != -1)
				return max(mx, e.second->weight);
		}
	}
	return -1;
}

int main() {
	ll n, m;
	cin >> n >> m;
	
	nodes.resize(n);
	edges.resize(m);
	
	for (ll i = 0; i < m; i++) {
		string weight;
		cin >> edges[i].a >> edges[i].b >> weight >> edges[i].inMST;
		edges[i].a--;
		edges[i].b--;
		
		nodes[edges[i].a].edges.emplace_back(edges[i].b, &edges[i]);
		nodes[edges[i].b].edges.emplace_back(edges[i].a, &edges[i]);
		
		edges[i].unknown = weight == "?";
		if (edges[i].unknown) {
			edges[i].weight = edges[i].inMST;
		} else {
			edges[i].weight = stol(weight);
		}
	}
	
	for (const Edge& e : edges) {
		if (!e.unknown)
			continue;
		
		ll w = e.weight;
		if (!e.inMST) {
			w = maxInPath(e.a, e.b, -1);
		}
		
		cout << w << "\n";
	}
}
