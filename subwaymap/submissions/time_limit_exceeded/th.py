#! python3
import math

n, m = map(int, input().split())
logn = 2 + int(math.log(n,2))
E = []
adj = [[] for _ in range(n)]

dist = {}

for _ in range(m):
    a, b, l, c = map(lambda s: 0 if s == "?" else int(s), input().split())
    a, b = min(a,b) - 1, max(a,b) - 1
    E.append((a,b,l,c))
    if c:
        adj[a].append(b)
        adj[b].append(a)
        dist[(a,b)] = l if l > 0 else 1
        dist[(b,a)] = l if l > 0 else 1

par = [[None] * logn for _ in range(n)] 
depth = [None] * n
length  = [None] * n

def run_dfs():
    cmds = [(0, None)]
    while cmds:
        v, p = cmds.pop()
        par[v][0] = p
        if p is not None:
            depth[v] = depth[p] + 1
        i = 1
        while par[v][i - 1] is not None:
            pi = par[v][i-1]
            pii = par[pi][i-1]
            par[v][i] = pii
            i += 1
        for u in adj[v]:
            if u != p:
                cmds.append((u, v))

def lca(v, u):
    if depth[v] < depth[u]:
        v, u = u, v
    assert depth[v] >= depth[u]
    for i in reversed(range(logn)):
        if par[v][i] is not None and depth[par[v][i]] >= depth[u]:
            v = par[v][i]
    assert depth[v] == depth[u]
    if v == u:
        return v
    for i in reversed(range(logn)):
        if par[v][i] !=  par[u][i]:
                v = par[v][i]
                u = par[u][i]
    return par[v][0];

depth[0] = 0

run_dfs()

for a, b, l, c in E:
    if l == 0:
        if c == 1:
            print (1)
        else:
            l = lca(a,b)
            #print (a,b ,l)
            L = []
            while a != l:
                p = par[a][0]
                L.append(dist[a,p])
                a = p
            while b != l:
                p = par[b][0]
                L.append(dist[b,p])
                b = p
            print (max(L))

#print (par)

#print (depth)

