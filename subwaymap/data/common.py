import sys
import random

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    assert default is not None, name
    return default

random.seed(int(cmdlinearg('seed', sys.argv[-1])))

class UnionFind:
    def __init__(self, n):
        self.par = [i for i in range(n)]
        self.rank = [0] * n
    
    def root(self, n):
        if self.par[n] != n:
            self.par[n] = self.root(self.par[n])
        return self.par[n]
    
    def merge(self, a, b):
        if self.rank[a] < self.rank[b]:
            self.par[a] = b
        else:
            self.par[b] = a
            self.rank[a] += self.rank[b] == self.rank[a]

def makeGraphFullyConnected(n, edges):
    uf = UnionFind(n)
    redundantEdges = []
    for i, e in enumerate(edges):
        rootA = uf.root(e[0])
        rootB = uf.root(e[1])
        if rootA != rootB:
            uf.merge(rootA, rootB)
        else:
            redundantEdges.append(i)
    
    uniqueRoots = list(set(map(lambda x: uf.root(x), range(n))))
    for i in range(1, len(uniqueRoots)):
        e = redundantEdges.pop()
        edges[e] = (uniqueRoots[i], random.choice(uniqueRoots[:i]), edges[e][2])

def findMST(n, edges):
    uf = UnionFind(n)
    ufp = [i for i in range(n)]
    edgesByWeight = sorted(range(len(edges)), key=lambda i: edges[i][2])
    mstEdges = []
    for i in edgesByWeight:
        rootA = uf.root(edges[i][0])
        rootB = uf.root(edges[i][1])
        if rootA != rootB:
            mstEdges.append(i)
            uf.merge(rootA, rootB)
    assert(len(mstEdges) == n - 1)
    return mstEdges
