#!/usr/bin/python3

from common import *
import random

n = int(cmdlinearg('n', 1000))
m = int(cmdlinearg('m', 2000))

edgs = []
D = set()
for i in range(1, n):
    edgs.append((i, i+1, random.randint(1, 10**6), 1))
    D.add((i, i+1))

for i in range(m - n+1):
    a = random.randint(1, n//2)
    b = random.randint(a+1, n)
    if (a, b) in D: continue
    D.add((a, b))

for a, b in D:
    if a+1 != b:
        edgs.append((a, b, "?", 0))
print('{} {}'.format(n, len(edgs)))
for u, v, w, c in edgs:
    print('{} {} {} {}'.format(u, v, w, c))
