#!/usr/bin/python3

from common import *
import random

n = int(cmdlinearg('n', 1000))
m = int(cmdlinearg('m', 2000))

edgs = []
D = set()
MID = n//3
for i in range(1, 2*MID):
    edgs.append((i, i+1, random.randint(1, 10**6), 1))

edgs.append((2*MID + 1, MID, random.randint(1, 10**6), 1))
for i in range(2*MID + 1, n):
    edgs.append((i, i+1, random.randint(1, 10**6), 1))
D = set([(min(x,y), max(x, y)) for x, y, _, _ in edgs])

while len(D) < m:
    a = random.randint(1, n)
    b = random.randint(1, n)
    if a == b: continue
    a, b = min(a, b), max(a, b)
    if (a, b) in D: continue
    D.add((a, b))
    edgs.append((a, b, "?", 0))

print('{} {}'.format(n, len(edgs)))
for u, v, w, c in edgs:
    print('{} {} {} {}'.format(u, v, w, c))
