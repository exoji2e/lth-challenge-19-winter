#!/usr/bin/env bash

USE_SCORING=0
. ../../testdata_tools/gen-acm.sh

ulimit -s unlimited

use_solution eae.cpp

compile gen_random.py
compile gen_line.py
compile gen_T.py
compile gen_star.py

limits nMax=100000 mMax=200000

sample 1
sample 2

tc rand1 gen_random n=6 m=8
tc rand2 gen_random n=12 m=12
tc rand3 gen_random n=25 m=40 unknownP=1
tc rand4 gen_random n=20 m=30 unknownP=0
tc rand5 gen_random n=200 m=3000
tc rand6 gen_random n=800 m=3000
tc rand7 gen_random n=1000 m=3000
tc rand8 gen_random n=1000 m=3000 unknownP=0.05
tc rand9 gen_random n=1000 m=3000 unknownP=0.8
tc rand10 gen_random n=50000 m=100000
tc rand11 gen_random n=50000 m=100000 unknownP=0.05
tc rand12 gen_random n=50000 m=100000 unknownP=0.9
tc rand13 gen_random n=77777 m=122222
tc rand14 gen_random n=100000 m=200000
tc rand15 gen_random n=100000 m=200000

tc line1 gen_line n=100 m=200
tc line2 gen_line n=10000 m=20000
tc line3 gen_line n=100000 m=200000

tc t1 gen_T n=100000 m=200000
tc t2 gen_T n=100000 m=200000
tc t3 gen_T n=100000 m=200000

tc star1 gen_star n=100000 m=200000
tc star2 gen_star n=100000 m=200000
