#!/usr/bin/python3

from common import *
import random

n = int(cmdlinearg('n', 1000))
m = int(cmdlinearg('m', 2000))
unknownP = float(cmdlinearg('unknownP', 0.2))

#Randomly generate edges
edges = set()
while len(edges) < m:
    a = random.randrange(n)
    b = random.randrange(n)
    if a != b:
        edges.add((min(a, b), max(a, b)))

edges = list(edges)

for i in range(len(edges)):
    edges[i] = tuple(list(edges[i]) + [random.randint(1, 10 ** 9)])

makeGraphFullyConnected(n, edges)

mst = set(findMST(n, edges))

print(n, len(edges))

for i, e in enumerate(edges):
    n1, n2, w = e
    if random.random() <= unknownP:
        w = "?"
    print(n1 + 1, n2 + 1, w, int(i in mst))
