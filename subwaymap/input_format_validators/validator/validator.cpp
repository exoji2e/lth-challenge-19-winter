#include "validator.h"
#include <set>
using namespace std;

constexpr int MAX_WEIGHT = 1E9;

void run() {
	int nMax = Arg("nMax");
	int mMax = Arg("mMax");
	
	constexpr int MAX_ABS_COORD = 1000000;
	
	int n = Int(1, nMax);
	Space();
	int m = Int(n - 1, mMax);
	Endl();
	
	set<pair<int, int>> edges;
	int numWithCable = 0;
	for (int i = 0; i < m; i++) {
		int a = Int(1, n);
		Space();
		int b = Int(1, n);
		Space();
		
		assert(a != b);
		
		if (!edges.emplace(min(a, b), max(a, b)).second) {
			die("duplicate edge");
		}
		
		string w = _token();
		if (w != "?") {
			assert(!w.empty());
			int wi = stoi(w);
			assert(wi > 0 && wi <= MAX_WEIGHT);
		}
		
		Space();
		if (Int(0, 1)) {
			numWithCable++;
		}
		Endl();
	}
	
	assert(numWithCable == n - 1);
}
